package main

import (
	"context"
	"errors"
	"strings"

	log "github.com/sirupsen/logrus"
	beluga "gitlab.com/mergetb/tech/beluga/pkg"
	api "gitlab.com/mergetb/tech/beluga/pkg/belugactld"
	stor "gitlab.com/mergetb/tech/stor"
)

type BelugactldServer struct {
}

func (b *BelugactldServer) SoftCycle(
	ctx context.Context, req *api.SoftCycleRequest,
) (*api.SoftCycleReply, error) {

	log.Infof("soft cycling %s", strings.Join(req.Nodeids, ", "))

	if err := b.doRequest(req.Nodeids, true, beluga.DoRequest_Cycle); err != nil {
		return nil, err
	}

	return &api.SoftCycleReply{}, nil
}

func (b *BelugactldServer) HardCycle(
	ctx context.Context, req *api.HardCycleRequest,
) (*api.HardCycleReply, error) {

	log.Infof("hard cycling %s", strings.Join(req.Nodeids, ", "))

	if err := b.doRequest(req.Nodeids, false, beluga.DoRequest_Cycle); err != nil {
		return nil, err
	}

	return &api.HardCycleReply{}, nil
}

func (b *BelugactldServer) PowerOn(
	ctx context.Context, req *api.PowerOnRequest,
) (*api.PowerOnReply, error) {

	log.Infof("power on %s", strings.Join(req.Nodeids, ", "))

	if err := b.doRequest(req.Nodeids, false, beluga.DoRequest_On); err != nil {
		return nil, err
	}

	return &api.PowerOnReply{}, nil
}

func (b *BelugactldServer) PowerOff(
	ctx context.Context, req *api.PowerOffRequest,
) (*api.PowerOffReply, error) {

	log.Infof("power off %s", strings.Join(req.Nodeids, ", "))

	if err := b.doRequest(req.Nodeids, false, beluga.DoRequest_Off); err != nil {
		return nil, err
	}

	return &api.PowerOffReply{}, nil
}

func (b *BelugactldServer) doRequest(
	nids []string, soft bool, action beluga.DoRequest_Action,
) error {

	for _, n := range nids {
		resName, _, err := b.nodeNames(n)
		if err != nil {
			return err
		}

		withBelugaClient(func(c beluga.BelugaClient) {
			_, e := c.Do(
				context.TODO(),
				&beluga.DoRequest{
					Action: action,
					Device: resName,
					Soft:   soft,
				},
			)
			err = e
		})
		if err != nil {
			return err
		}
	}

	return nil
}

func (b *BelugactldServer) Status(
	ctx context.Context, req *api.StatusRequest,
) (*api.StatusReply, error) {

	log.Infof("status request %s", strings.Join(req.Nodeids, ", "))

	reply := &api.StatusReply{}

	for _, n := range req.Nodeids {
		resName, xpName, err := b.nodeNames(n)
		if err != nil {
			return nil, err
		}

		log.Infof("Getting status for: %s/%s", resName, xpName)

		var stat string
		withBelugaClient(func(c beluga.BelugaClient) {
			resp, e := c.Get(
				context.TODO(),
				&beluga.StatusRequest{
					Device: resName,
				},
			)

			log.Infof("status - resp: %v, err: %v", resp, e)

			stat = resp.State.String()
			err = e
		})

		if err != nil {
			log.Errorf("beluga client: %#v", err)
			return nil, err
		}

		reply.Status = append(reply.Status, &api.NodeStatus{
			Resource: resName,
			Xpname:   xpName,
			Status:   stat,
		})

		log.Infof("current status reply: %#v", reply)
	}

	return reply, nil
}

// Return the resource and experiment node name given either.
func (b *BelugactldServer) nodeNames(nid string) (string, string, error) {

	// look for the existence of either key.
	xpi := &xpInfo{Xpname: nid}
	err := stor.Read(xpi)
	// Not found is ok. We'll check for resource name next
	// as the user can specify either xp or resource name.
	if errors.Is(err, stor.ErrNotFound) {

		ri := &resInfo{Resource: nid}
		err = stor.Read(ri)
		if errors.Is(err, stor.ErrNotFound) {
			// not found under either key, this is not our node or is not a node at all
			return "", "", ErrResourceNotFound
		}
		if err != nil {
			return "", "", err // etcd/stor error
		}

		return ri.Resource, ri.Xpname, nil
	}
	if err != nil {
		return "", "", err // etcd/stor error
	}

	// found under xpname.
	return xpi.Resource, xpi.Xpname, nil
}
