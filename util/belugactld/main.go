package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"time"

	log "github.com/sirupsen/logrus"
	beluga "gitlab.com/mergetb/tech/beluga/pkg"
	api "gitlab.com/mergetb/tech/beluga/pkg/belugactld"
	stor "gitlab.com/mergetb/tech/stor"
	"google.golang.org/grpc"
	"gopkg.in/yaml.v2"
)

var (
	mzid string

	server = &BelugactldServer{}

	ErrNotImplemented   = errors.New("not implmented")
	ErrResourceNotFound = errors.New("node/resource not found")
)

func init() {

	// setup etcd connection.
	stor.SetConfig(stor.Config{
		Address: "127.0.0.1",
		Port:    2379,
		TLS:     nil,
		Quantum: 200 * time.Millisecond,
		Timeout: 10 * time.Second,
	})

}

func getBelugadEndpoint() (string, error) {

	// cfgPath assumes that the container has been mounted with the cogs runtime
	// configuration file.  The default location is /etc/cogs/runtime.yml
	cfgPath := "/etc/cogs/runtime.yml"

	data, err := ioutil.ReadFile(cfgPath)
	if err != nil {
		return "", err
	}

	type runtimeCfg struct {
		Beluga struct {
			Address string `yaml:"address"`
			Port    string `yaml:"port"`
		}
	}

	cfg := &runtimeCfg{}

	err = yaml.Unmarshal(data, cfg)
	if err != nil {
		return "", fmt.Errorf("beluga cfg parse error: %w", err)
	}

	endpoint := fmt.Sprintf("%s:%s", cfg.Beluga.Address, cfg.Beluga.Port)

	log.Debug("found belugad endpoint: %s", endpoint)

	return endpoint, nil
}

func withBelugaClient(f func(c beluga.BelugaClient)) {

	server, err := getBelugadEndpoint()
	if err != nil {
		log.Errorf("read belugad endpoint: %+v", err)
		return
	}

	conn, err := grpc.Dial(fmt.Sprintf("%s", server), grpc.WithInsecure())
	if err != nil {
		log.Errorf("belugad connection failed: %#v", err)
		return
	}
	defer conn.Close()

	c := beluga.NewBelugaClient(conn)

	f(c)
}

func main() {

	port := 6941
	log.SetLevel(log.InfoLevel)

	l, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%d", port))
	if err != nil {
		log.Fatalf("net listen: %v", err)
	}

	log.Infof("belugactld starting - version %s", beluga.Version)
	log.Infof("listening on 0.0.0.0:%d", port)

	grpcServer := grpc.NewServer()
	api.RegisterBelugactldServer(grpcServer, server)

	go runManager() // start the mangement API server

	grpcServer.Serve(l)
}
