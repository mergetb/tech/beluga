package main

import (
	"context"
	"flag"
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/beluga/pkg/belugactld"
	"gitlab.com/mergetb/tech/stor"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

type BelugactldManageServer struct {
}

const (
	mgmtPort = 27001
	svcName  = "beluga"
)

//
// GRPC Server boilerplate
//
// The runManager code is essentially just copied from foundryd. May want to factor this code out into
// an infrapod util library
//
var (
	noauth = flag.Bool(
		"no-auth", false, "do not authenticate management connections")
	mEndpoint = flag.String(
		"manage-endpoint",
		fmt.Sprintf("0.0.0.0:%d", mgmtPort),
		"listening endpoint")
	cert = flag.String(
		"cert",
		fmt.Sprintf("/etc/%s/manage.pem", svcName),
		fmt.Sprintf("%s TLS cert", svcName),
	)
	key = flag.String(
		"key",
		fmt.Sprintf("/etc/%s/manage-key.pem", svcName),
		fmt.Sprintf("%s TLS key", svcName),
	)
	maxMsgSize = flag.Int("maxsize", 4, "grpc and etcd max message size (MB)")
)

func runManager() {

	m := &BelugactldManageServer{}

	fields := log.Fields{
		"noauth":           *noauth,
		"manage endpoint":  *mEndpoint,
		"cert":             *cert,
		"key":              *key,
		"max message size": *maxMsgSize,
	}

	servOpts := []grpc.ServerOption{
		grpc.MaxRecvMsgSize(*maxMsgSize * 1024 * 1024),
		grpc.MaxSendMsgSize(*maxMsgSize * 1024 * 1024),
	}

	var gs *grpc.Server
	if !*noauth {
		creds, err := credentials.NewServerTLSFromFile(*cert, *key)
		if err != nil {
			log.Fatal(err)
		}
		servOpts = append(servOpts, grpc.Creds(creds))
	}
	gs = grpc.NewServer(servOpts...)

	api.RegisterBelugactldManageServer(gs, m)
	l, err := net.Listen("tcp", *mEndpoint)
	if err != nil {
		log.Fatal(err)
	}

	log.WithFields(fields).Infof("listening on %s", *mEndpoint)
	gs.Serve(l)
}

//
// Implement the stor interface to store node info data.
//
// We keep the two names of the resource (xp name and resource name)
// as separate keys for quick and easy access.
//
type resInfo struct {
	Xpname   string
	Resource string
	ver      int64
}

func (n *resInfo) Key() string        { return "/belugactl/nodes/resource/" + n.Resource }
func (n *resInfo) GetVersion() int64  { return n.ver }
func (n *resInfo) SetVersion(v int64) { n.ver = v }
func (n *resInfo) Value() interface{} { return n }

type xpInfo struct {
	Xpname   string
	Resource string
	ver      int64
}

func (n *xpInfo) Key() string        { return "/belugactl/nodes/xpname/" + n.Xpname }
func (n *xpInfo) GetVersion() int64  { return n.ver }
func (n *xpInfo) SetVersion(v int64) { n.ver = v }
func (n *xpInfo) Value() interface{} { return n }

//
// Implement the GRPC API
//
func (b *BelugactldManageServer) SetNodeInfo(
	ctx context.Context, req *api.SetNodeInfoRequest,
) (*api.SetNodeInfoReply, error) {

	log.Debugf("SetNodeInfo %#v", req)

	xpi := &xpInfo{Xpname: req.Xpname, Resource: req.Resource}
	ri := &resInfo{Xpname: req.Xpname, Resource: req.Resource}
	objs := []stor.Object{xpi, ri}

	err := stor.WriteObjects(objs, false)
	if err != nil {
		return nil, err
	}

	return &api.SetNodeInfoReply{}, nil
}

func (b *BelugactldManageServer) DeleteNodeInfo(
	ctx context.Context, req *api.DeleteNodeInfoRequest,
) (*api.DeleteNodeInfoReply, error) {

	log.Debugf("DeleteNodeInfo %#v", req)

	xpi := &xpInfo{Xpname: req.Xpname, Resource: req.Resource}
	ri := &resInfo{Xpname: req.Xpname, Resource: req.Resource}
	objs := []stor.Object{xpi, ri}

	err := stor.DeleteObjects(objs)
	if err != nil {
		return nil, err
	}

	return &api.DeleteNodeInfoReply{}, nil
}
