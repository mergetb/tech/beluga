package main

import (
	"context"
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/mergetb/tech/beluga/pkg"
)

var (
	verbose bool
	server  string
)

var tw = tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)

func init() {
	root.PersistentFlags().StringVarP(
		&server, "server", "S", "",
		"GRPC endpoint. Will read /etc/beluga.yml for default value. Default to localhost:5402",
	)
	root.PersistentFlags().BoolVarP(
		&verbose, "verbose", "v", false, "Verbose output if true",
	)

	if server == "" {
		server = defaultServer()
	}
}

var root = &cobra.Command{
	Use:   "beluga",
	Short: "Beluga client utility",
}

func main() {
	log.SetFlags(0)

	version := &cobra.Command{
		Use:   "version",
		Short: "Show beluga version",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			log.Print(beluga.Version)
		},
	}
	root.AddCommand(version)

	var soft bool

	off := &cobra.Command{
		Use:   "off [device]",
		Short: "Turn off a device",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			off(args[0], soft)
		},
	}
	off.Flags().BoolVarP(&soft, "soft", "s", false, "use soft version of command")
	root.AddCommand(off)

	on := &cobra.Command{
		Use:   "on [device]",
		Short: "Turn on a device",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			on(args[0], soft)
		},
	}
	on.Flags().BoolVarP(&soft, "soft", "s", false, "use soft version of command")
	root.AddCommand(on)

	cycle := &cobra.Command{
		Use:   "cycle [device]",
		Short: "Cycle a device",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			cycle(args[0], soft)
		},
	}
	cycle.Flags().BoolVarP(&soft, "soft", "s", false, "use soft version of command")
	root.AddCommand(cycle)

	status := &cobra.Command{
		Use:   "status [device]",
		Short: "Get device power status",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			getStatus(args[0])
		},
	}
	root.AddCommand(status)

	list := &cobra.Command{
		Use:   "list",
		Short: "List devices",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listDevices()
		},
	}
	root.AddCommand(list)

	root.Execute()
}

func off(device string, soft bool) {
	withBelugaClient(func(c beluga.BelugaClient) {

		_, err := c.Do(context.TODO(), &beluga.DoRequest{
			Action: beluga.DoRequest_Off,
			Device: device,
			Soft:   soft,
		})
		if err != nil {
			grpcFatal(err)
		}

	})
}

func on(device string, soft bool) {
	withBelugaClient(func(c beluga.BelugaClient) {

		_, err := c.Do(context.TODO(), &beluga.DoRequest{
			Action: beluga.DoRequest_On,
			Device: device,
			Soft:   soft,
		})
		if err != nil {
			grpcFatal(err)
		}

	})
}

func cycle(device string, soft bool) {
	withBelugaClient(func(c beluga.BelugaClient) {

		_, err := c.Do(context.TODO(), &beluga.DoRequest{
			Action: beluga.DoRequest_Cycle,
			Device: device,
			Soft:   soft,
		})
		if err != nil {
			grpcFatal(err)
		}

	})
}

func getStatus(device string) {
	withBelugaClient(func(c beluga.BelugaClient) {

		resp, err := c.Get(context.TODO(), &beluga.StatusRequest{
			Device: device,
		})
		if err != nil {
			grpcFatal(err)
		}

		log.Print(resp.State.String())

	})

}

func listDevices() {
	withBelugaClient(func(c beluga.BelugaClient) {

		resp, err := c.List(context.TODO(), &beluga.ListRequest{})
		if err != nil {
			grpcFatal(err)
		}

		sort.Slice(resp.Devices, func(i, j int) bool {
			return resp.Devices[i].Device < resp.Devices[j].Device
		})

		for _, d := range resp.Devices {
			fmt.Fprintf(tw, "%s\t%s\n", d.Device, d.Controller)
		}

		tw.Flush()

	})

}

func withBelugaClient(f func(c beluga.BelugaClient)) {

	conn, err := grpc.Dial(fmt.Sprintf("%s", server), grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	c := beluga.NewBelugaClient(conn)

	f(c)

}

func grpcFatal(err error) {

	s, ok := status.FromError(err)
	if !ok {
		log.Fatal(err)
	}
	if s.Code() == codes.Unavailable {
		log.Printf("\nServer is set to %s. Is this correct?\n", server)
	}
	log.Fatal(s.Message())

}

func defaultServer() string {

	type cfg struct {
		Listen string
	}

	bytes, err := ioutil.ReadFile("/etc/beluga.yml")
	if err != nil {
		return "localhost:5402"
	}

	var c cfg

	err = yaml.Unmarshal(bytes, &c)
	if err != nil {
		return "localhost:5402"
	}

	return c.Listen
}
