# Wayward Beluga

This is an extensible power controller that abstracts the lovely details of PDUs and various other power control devices away from your core infrastructure. Beluga gives you a nice simple gRPC interface for all your power control needs. 


## Interface

Beluga has a very simple interface

```protobuf
message DoRequest {
  enum Action {
    Off = 0;
    On = 1;
    Cycle = 2;
  }
  Action action = 1;
  string device = 2;
}
message DoResponse { }

message StatusRequest{
  string device = 1;
}
message StatusResponse {
  enum State {
    Off = 0;
    On = 1;
  }
  State state = 1;
}

service Beluga {
  rpc Do(Request) returns (Response);
  rpc Get(StatusRequest) returns (StatusResponse);
}
```

## Plugins

Beluga has a plugin model. We currently support the following.

- [Raven](https://gitlab.com/rygoo/raven) virtual machines.
- [APC](https://www.apc.com/shop/us/en/categories/power-distribution/rack-power-distribution/switched-rack-pdu/N-17k76am) Switched Rack PDUs.
- [NCD](https://store.ncd.io/product/ethernet-interface-adapter-for-ncd-iot-devices-lantronix-xport/) XPort/Fusion devices.

Contributions to new devices are welcome. You can contribute a new plugin by adding a file to `/pkg` with the name of your plugin and implementing the controller interface. Then adding your plugin to the [list](https://gitlab.com/mergetb/tech/beluga/blob/master/pkg/beluga.go#L14).

```go
type Controller interface {
	Init()
	Do(x *Request) error
}
```
