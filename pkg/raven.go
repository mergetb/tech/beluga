package beluga

import (
	"fmt"
	"os/exec"
	"strings"

	"github.com/mitchellh/mapstructure"

	log "github.com/sirupsen/logrus"
)

var ravenconfig *RavenConfig = &RavenConfig{}

type RavenConfig struct {
	Topodir string
}

type Raven struct{}

func (r *Raven) Init() {
	cfg, ok := config.Controllers["raven"]
	if !ok {
		log.Fatal("no raven controller config found")
	}

	err := mapstructure.Decode(cfg, ravenconfig)
	if err != nil {
		log.Fatal("could not decode raven config")
	}
}

func (r *Raven) Do(x *DoRequest) error {

	var cmd *exec.Cmd
	switch x.Action {
	case DoRequest_Off:
		cmd = exec.Command("rvn", "powerdown", x.Device)
	case DoRequest_On:
		cmd = exec.Command("rvn", "powerup", x.Device)
	case DoRequest_Cycle:
		cmd = exec.Command("rvn", "reboot", x.Device)
	}

	if cmd != nil {
		cmd.Dir = ravenconfig.Topodir
	}

	out, err := cmd.CombinedOutput()
	if err != nil {
		log.WithFields(log.Fields{
			"err":    err,
			"action": x.Action.String(),
			"device": x.Device,
			"out":    out,
		}).Error("raven command failed")
	}

	return nil
}

func (r *Raven) Get(x *StatusRequest) (StatusResponse_State, error) {

	// just troll though `rvn status` looking for our node.
	// this is not an ideal solution, but works "for now"

	cmd := exec.Command("rvn", "status")

	if cmd != nil {
		cmd.Dir = ravenconfig.Topodir
	}

	// rvn status output is on stderr for some reason
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Error("exec rvn status failed")
		return StatusResponse_Unknown, fmt.Errorf("exec rvn status error: %s", err)
	}

	tokens := strings.Split(string(out), " ")
	status := ""

	for i, t := range tokens {
		if t == x.Device {
			log.Infof("found %s status: %s\n", x.Device, tokens[i+1])
			status = tokens[i+1]
			break
		}
	}

	if status == "running" {
		return StatusResponse_On, nil
	} else if status == "off" {
		return StatusResponse_Off, nil
	}

	log.Infof("status for %s not found", x.Device)

	return StatusResponse_Unknown, nil
}
