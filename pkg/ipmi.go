package beluga

import (
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"github.com/vmware/goipmi"
)

var ipmiconfig = &IPMIConfig{}

// IPMIAddr is how we address the IPMI address
type IPMIAddr struct {
	IP       string // can be hostname as well
	Username string
	Password string
}

// IPMIConfig is a map of device name to settings
type IPMIConfig struct {
	Devices map[string]IPMIAddr
}

// IPMI struct implementing the interface
type IPMI struct {
}

// Init will parse the config and track IPMI devices
func (i *IPMI) Init() {
	cfg, ok := config.Controllers["ipmi"]
	if !ok {
		log.Fatal("no ipmi config found")
	}

	err := mapstructure.Decode(cfg, ipmiconfig)
	if err != nil {
		log.Fatal("could not decode ipmi config")
	}
}

func ipmiClient(device string) (*ipmi.Client, error) {

	dsettings := ipmiconfig.Devices[device]

	conn := &ipmi.Connection{
		Hostname:  dsettings.IP,
		Username:  dsettings.Username,
		Password:  dsettings.Password,
		Interface: "lanplus",
		// Path
		// Port
	}

	client, err := ipmi.NewClient(conn)
	if err != nil {
		log.WithError(err).Error("failed to create ipmi client")
		return nil, err
	}

	return client, nil

}

// Do implements the beluga set of Do functionality
func (i *IPMI) Do(do *DoRequest) error {

	client, err := ipmiClient(do.Device)
	if err != nil {
		return err
	}

	switch do.Action {
	case DoRequest_Off:
		err = client.Control(ipmi.ControlPowerDown)
		if err != nil {
			log.WithError(err).Error("failed to power off ipmi device")
			return err
		}
		return nil
	case DoRequest_On:
		err = client.Control(ipmi.ControlPowerUp)
		if err != nil {
			log.WithError(err).Error("failed to power on ipmi device")
			return err
		}
		return nil
	case DoRequest_Cycle:
		err = client.Control(ipmi.ControlPowerCycle)
		if err != nil {
			log.WithError(err).Error("failed to power cycle ipmi device")
			return err
		}
		return nil
	}

	return nil

}

// Get implements the beluga set of Get requests
func (i *IPMI) Get(sr *StatusRequest) (StatusResponse_State, error) {

	client, err := ipmiClient(sr.Device)
	if err != nil {
		return StatusResponse_Unknown, err
	}

	request := &ipmi.Request{
		NetworkFunction: ipmi.NetworkFunctionChassis,
		Command:         ipmi.CommandChassisStatus,
		Data:            ipmi.ChassisStatusRequest{},
	}
	var response = &ipmi.ChassisStatusResponse{}

	err = client.Send(request, response)
	if err != nil {
		log.WithError(err).Error("failed to query ipmi device status")
		return StatusResponse_Unknown, err
	}

	if response.IsSystemPowerOn() {
		return StatusResponse_On, nil
	}
	return StatusResponse_Off, nil
}
