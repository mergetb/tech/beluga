package beluga

import (
	"fmt"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/dcomptb/apc/pkg"
)

var apcconfig *ApcConfig = &ApcConfig{}

type ApcConfig struct {
	Devices map[string]ApcDevice
}

type ApcDevice struct {
	Pdu     string
	Outlets []int `yaml:"outlets,flow"`
}

type Apc struct{}

func (a *Apc) Init() {
	cfg, ok := config.Controllers["apc"]
	if !ok {
		log.Fatal("no apc config found")
	}

	err := mapstructure.Decode(cfg, apcconfig)
	if err != nil {
		log.Fatal("could not decode apc config")
	}
}

func (m *Apc) Do(x *DoRequest) error {

	dev, ok := apcconfig.Devices[x.Device]
	if !ok {
		return fmt.Errorf("device %s not found", x.Device)
	}

	fields := log.Fields{"device": x.Device}

	switch x.Action {
	case DoRequest_Off:
		for _, outlet := range dev.Outlets {

			fields["outlet"] = outlet
			log.WithFields(fields).Info("off")

			err := apc.Off(dev.Pdu, outlet)
			if err != nil {
				log.WithError(err).WithFields(fields).Error("off")
				return err
			}

		}
	case DoRequest_On:
		for _, outlet := range dev.Outlets {

			fields["outlet"] = outlet
			log.WithFields(fields).Info("on")

			err := apc.On(dev.Pdu, outlet)
			if err != nil {
				log.WithError(err).WithFields(fields).Error("off")
				return err
			}

		}
	case DoRequest_Cycle:
		//TODO
		// this is a broken interface for multiple outlets as it will just cycle
		// sequentially. The APC library needs to be updated to accept multiple
		// outlets
		for _, outlet := range dev.Outlets {

			fields["outlet"] = outlet
			log.WithFields(fields).Info("cycle")
			err := apc.Cycle(dev.Pdu, outlet)

			if err != nil {
				log.WithError(err).WithFields(fields).Error("off")
				return err
			}

		}
	}

	return nil

}

func (r *Apc) Get(x *StatusRequest) (StatusResponse_State, error) {

	dev, ok := apcconfig.Devices[x.Device]
	if !ok {
		return StatusResponse_Unknown,
			fmt.Errorf("device %s not found", x.Device)
	}

	for _, outlet := range dev.Outlets {
		state, err := apc.State(dev.Pdu, outlet)
		if err != nil {
			return StatusResponse_Unknown, err
		}
		if state == apc.OutletOn {
			return StatusResponse_On, nil
		}

	}

	return StatusResponse_Off, nil

}
