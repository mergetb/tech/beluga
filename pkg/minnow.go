package beluga

import (
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"

	mc "gitlab.com/dcomptb/minnowchassis/pkg"
)

var minnowconfig *MinnowConfig = &MinnowConfig{}

type MinnowAddr struct {
	Chassis string
	Index   byte
}

type MinnowConfig struct {
	Devices map[string]MinnowAddr
}

type Minnow struct {
	controllers map[string]*mc.RelayController

	mtx sync.Mutex
}

func (m *Minnow) Init() {

	m.controllers = make(map[string]*mc.RelayController)

	cfg, ok := config.Controllers["minnow"]
	if !ok {
		log.Fatal("no minnow config found")
	}

	err := mapstructure.Decode(cfg, minnowconfig)
	if err != nil {
		log.Fatal("could not decode minnow config")
	}
}

func (m *Minnow) Do(x *DoRequest) error {

	dev, ok := minnowconfig.Devices[x.Device]
	if !ok {
		return fmt.Errorf("device %s not found", x.Device)
	}

	switch x.Action {
	case DoRequest_Off:
		if x.Soft {
			return m.softOff(dev.Chassis, dev.Index)
		} else {
			return m.off(dev.Chassis, dev.Index)
		}
	case DoRequest_On:
		if x.Soft {
			return m.softOn(dev.Chassis, dev.Index)
		} else {
			return m.on(dev.Chassis, dev.Index)
		}
	case DoRequest_Cycle:
		if x.Soft {
			return m.softCycle(dev.Chassis, dev.Index)
		} else {
			return m.cycle(dev.Chassis, dev.Index)
		}
	}

	return nil
}

func (m *Minnow) Get(x *StatusRequest) (StatusResponse_State, error) {

	dev, ok := minnowconfig.Devices[x.Device]
	if !ok {
		return StatusResponse_Unknown,
			fmt.Errorf("device %s not found", x.Device)
	}

	resp := StatusResponse_Unknown
	err := m.withController(dev.Chassis, func(c *mc.RelayController) error {

		rly, s, err := c.CurrentState(dev.Index)
		if err != nil {
			return err
		}

		if (rly.Value & s) != 0 {
			resp = StatusResponse_On
			return nil
		} else {
			resp = StatusResponse_Off
			return nil
		}

	})

	if err != nil {
		return StatusResponse_Unknown, err
	}

	return resp, nil

}

func (m *Minnow) off(host string, i byte) error {
	return m.withController(host, func(c *mc.RelayController) error {
		return c.Off(i)
	})
}

func (m *Minnow) on(host string, i byte) error {
	return m.withController(host, func(c *mc.RelayController) error {
		return c.On(i)
	})
}

func (m *Minnow) cycle(host string, i byte) error {
	return m.withController(host, func(c *mc.RelayController) error {
		err := c.Off(i)
		if err != nil {
			return err
		}
		return c.On(i)
	})
}

func (m *Minnow) softOff(host string, i byte) error {

	return m.withController(host, func(c *mc.RelayController) error {

		/* push the power button for 6 seconds, this always turns off a minnow,
		* independent of it's current state */
		err := c.On(i + 100)
		if err != nil {
			return err
		}
		time.Sleep(6 * time.Second)
		return c.Off(i + 100)

	})
}

func (m *Minnow) softOn(host string, i byte) error {
	return m.withController(host, func(c *mc.RelayController) error {

		/* push the power button for half a second */
		err := c.On(i + 100)
		if err != nil {
			return err
		}
		time.Sleep(500 * time.Millisecond)
		return c.Off(i + 100)

	})
}

func (m *Minnow) softCycle(host string, i byte) error {

	/* cycle by off then on */
	err := m.softOff(host, i)
	if err != nil {
		return err
	}
	return m.softOn(host, i)

}

func (m *Minnow) withController(
	host string, f func(*mc.RelayController) error) error {

	m.mtx.Lock()
	x, ok := m.controllers[host]
	if !ok {
		x = mc.NewMC24(host)
		m.controllers[host] = x

		//TODO remove in favor of etcd
		if _, err := os.Stat(".state.json"); !os.IsNotExist(err) {
			x.LoadFromFile(".state.json")
		}
	}
	m.mtx.Unlock()

	err := f(x)
	if err != nil {
		return err
	}

	//TODO remove in favor of etcd
	err = x.SaveToFile(".state.json")
	if err != nil {
		return err
	}

	return nil

}
