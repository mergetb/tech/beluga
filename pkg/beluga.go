package beluga

import (
	"flag"
	"fmt"
	"io/ioutil"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

var Version = "undefined"

var config *Config
var configfile string = "/etc/beluga.yml"
var controllers map[string]Controller = map[string]Controller{
	"raven":  &Raven{},
	"minnow": &Minnow{},
	"apc":    &Apc{},
	"ipmi":   &IPMI{},
}

func ServerInit() {
	flag.StringVar(&configfile, "config", "/etc/beluga.yml",
		"location of config file")
	flag.Parse()
	loadConfig()
	controller_init()
}

// DoWith does the specified request with the provided controller name. The
// controller is looked up in the plugin table. If it is not found an error
// is returned. Otherwise the error condition of the underlying plugin is
// returned.
func DoWith(controller string, x *DoRequest) error {

	c, ok := controllers[controller]

	if !ok {
		log.WithFields(log.Fields{
			"controller": controller,
		}).Error("controller not found")
		return fmt.Errorf("controller not found")
	}

	err := c.Do(x)
	if err != nil {
		return err
	}

	return nil
}

func GetFrom(controller string, x *StatusRequest) (StatusResponse_State, error) {

	c, ok := controllers[controller]

	if !ok {
		log.WithFields(log.Fields{
			"controller": controller,
		}).Error("controller not found")
		return StatusResponse_Unknown, fmt.Errorf("controller not found")
	}

	return c.Get(x)

}

// Retrieve the beluga runtime configuration
func GetConfig() Config {
	return *config
}

func controller_init() {

	inuse := make(map[string]byte)
	for _, x := range config.Devices {
		inuse[x.Controller] = 1
	}

	for x, _ := range inuse {
		controllers[x].Init()
	}

}

type DeviceConfig struct {
	Controller string
}

type Config struct {
	Listen      string
	Devices     map[string]DeviceConfig
	Controllers map[string]interface{}
}

type Controller interface {
	Init()
	Do(*DoRequest) error
	Get(*StatusRequest) (StatusResponse_State, error)
}

func loadConfig() error {

	buf, err := ioutil.ReadFile(configfile)
	if err != nil {
		log.WithFields(log.Fields{
			"err":      err,
			"filename": configfile,
		}).Fatal("failed to load config")
		return fmt.Errorf("failed to load config")
	}

	err = yaml.Unmarshal(buf, &config)
	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Fatal("failed to parse config")
		return fmt.Errorf("failed to parse config")
	}

	return nil
}
