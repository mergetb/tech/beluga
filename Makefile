all: build/belugad build/beluga build/belugactld

prefix ?= /usr/local

VERSION = $(shell git describe --always --long --dirty)
LDFLAGS = "-X gitlab.com/mergetb/tech/beluga/pkg.Version=$(VERSION)"

build/belugad: svc/main.go pkg/*.go pkg/beluga.pb.go | build
	$(go-build)

build/beluga: util/beluga/main.go pkg/*.go pkg/beluga.pb.go | build
	$(go-build)

build/belugactld: util/belugactld/*.go pkg/belugactld/belugactld.pb.go pkg/beluga.pb.go | build
	$(go-build)

belugactld-ctr: ctr/belugactld.dock build/belugactld | .tools
	$(call docker-build,belugactld)

protoc-gen-go=.tools/protoc-gen-go
$(protoc-gen-go): | .tools
	$(QUIET) GOBIN=`pwd`/.tools go install github.com/golang/protobuf/protoc-gen-go

.PHONY: tools
tools: $(protoc-gen-go)

.tools:
	$(QUIET) mkdir .tools

pkg/beluga.pb.go: pkg/beluga.proto | $(protoc-gen-go)
	$(protoc-build)

pkg/belugactld/belugactld.pb.go: pkg/belugactld/belugactld.proto | $(protoc-gen-go)
	$(protoc-build)

build:
	$(QUIET) mkdir -p build

.PHONY: clean
clean:
	$(QUIET) rm -rf build

.PHONY: distclean
distclean: clean
	$(QUIET) find . -name "*.pb.go" -print | xargs -n 1 rm -f

BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

QUIET=@
ifeq ($(V),1)
	QUIET=
endif

# Where to push containers to (only belugactld at the moment).
REG ?= docker.io
ORG ?= mergetb
TAG ?= latest

define build-slug
	@printf "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)\n"
endef

define go-build
	$(call build-slug,go)
	$(QUIET) go build -ldflags=${LDFLAGS} -o $@ $(dir $<)/*
endef

define protoc-build
	$(call build-slug,protoc)
	$(QUIET) PATH=./.tools:$$PATH protoc \
		-I . \
		-I ./$(dir $@) \
		./$< \
		--go_out=plugins=grpc:.
endef

define docker-build
	$(call build-slug,docker)
	$(QUIET) docker build --no-cache -f $< -t $(REG)/$(ORG)/$1:$(TAG) .
	$(if $(PUSH),$(call docker-push,$1))
endef

define docker-push
	$(call build-slug,push)
	$(QUIET) docker push $(REG)/$(ORG)/$1:$(TAG)
endef

.PHONY: install
install: build/beluga build/belugad
	install -D build/beluga $(DESTDIR)$(prefix)/bin/beluga
	install -D build/belugad $(DESTDIR)$(prefix)/bin/belugad
