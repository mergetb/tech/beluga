package main

import (
	"context"
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	"gitlab.com/mergetb/tech/beluga/pkg"
)

func main() {

	log.Printf("beluga %s", beluga.Version)

	beluga.ServerInit()
	log.Printf("the beluga listens on %s", beluga.GetConfig().Listen)

	s := grpc.NewServer()
	beluga.RegisterBelugaServer(s, &server{})
	l, err := net.Listen("tcp", beluga.GetConfig().Listen)
	if err != nil {
		log.Fatal(err)
	}
	s.Serve(l)

}

type server struct{}

func (b *server) Do(
	ctx context.Context, x *beluga.DoRequest,
) (*beluga.DoResponse, error) {

	dc, ok := beluga.GetConfig().Devices[x.Device]
	if !ok {
		log.WithFields(log.Fields{
			"device": x.Device,
		}).Warning("device not found")
		return nil, fmt.Errorf("device not found")
	}

	err := beluga.DoWith(dc.Controller, x)
	if err != nil {
		return nil, err
	}

	return &beluga.DoResponse{}, nil

}

func (b *server) Get(
	ctx context.Context, x *beluga.StatusRequest,
) (*beluga.StatusResponse, error) {

	dc, ok := beluga.GetConfig().Devices[x.Device]
	if !ok {
		log.WithFields(log.Fields{
			"device": x.Device,
		}).Warning("device not found")
		return nil, fmt.Errorf("device not found")
	}

	state, err := beluga.GetFrom(dc.Controller, x)
	if err != nil {
		return nil, err
	}

	return &beluga.StatusResponse{
		State: state,
	}, nil

}

func (b *server) List(
	ctx context.Context, x *beluga.ListRequest,
) (*beluga.ListResponse, error) {

	response := &beluga.ListResponse{}

	cfg := beluga.GetConfig()
	for name, config := range cfg.Devices {
		response.Devices = append(response.Devices, &beluga.ListItem{
			Device:     name,
			Controller: config.Controller,
		})
	}

	return response, nil

}

func (b *server) Health(
	ctx context.Context, x *beluga.HealthRequest,
) (*beluga.HealthResponse, error) {

	return &beluga.HealthResponse{}, nil

}
