#!/bin/bash

TARGET=${TARGET:-amd64}
DEBUILD_ARGS=${DEBUILD_ARGS:-""}

rm -f build/beluga*.build*
rm -f build/beluga*.change
rm -f build/beluga*.deb

debuild -e V=1 -e prefix=/usr $DEBUILD_ARGS -i -us -uc -b

mv ../beluga*.build* build/
mv ../beluga*.changes build/
mv ../beluga*.deb build/
