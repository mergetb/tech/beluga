#!/bin/bash

set -e

docker build -f debian/builder.dock -t beluga-builder .
docker run -v `pwd`:/canopy beluga-builder /canopy/build-deb.sh
